<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$vote_id = get('id');
$page_path = "/admin/votes/report.php?id={$vote_id}";

$time = time();
$data = DB::row("SELECT * FROM `votes` WHERE `vote_id`='{$vote_id}'");

if ($time >= strtotime($data['start_time']) && $time <= strtotime($data['end_time'])) {
    $vote_status = true;
} else {
    $vote_status = false;
}

$items = DB::result("SELECT * FROM `candidates` WHERE `vote_id`='{$vote_id}'");

$scores = DB::result("SELECT 
`candidate_id`,
COUNT(`candidate_id`) AS count_score
FROM `vote_actions`
WHERE `vote_id`='{$vote_id}'
GROUP BY `candidate_id`");

foreach ($items as &$item) {
    foreach ($scores as $score) {
        if ($item['candidate_id'] === $score['candidate_id']) {
            $item['score'] = $score['count_score'];
        }
    }
    unset($item);
}

$score_not_vote = DB::row("SELECT 
SUM(`not_vote`) AS score_not_vote
FROM `vote_actions` 
WHERE `vote_id`='{$vote_id}'
GROUP BY `vote_id`");

ob_start();
?>
<?= showAlert() ?>
<h3>รายละเอียด</h3>
<p>
    เลือกตั้ง: <?= $data['vote_name'] ?>
    <br>
    วันเวลาเริ่มเลือกตั้ง: <?= $data['start_time'] ?>
    <br>
    วันเวลาสิ้นสุดเลือกตั้ง: <?= $data['end_time'] ?>
    <br>
    สถานะ: <?= $vote_status ? 'เปิด' : 'ปิด' ?>
</p>

<h3>รายการคะแนนเลือกตั้ง</h3>
<table>
    <thead>
        <tr>
            <th>หมายเลขผู้ลงเลือกตั้ง</th>
            <th>ภาพผู้ลงเลือกตั้ง</th>
            <th>ชื่อผู้ลงเลือกตั้ง</th>
            <th>นามสกุลผู้ลงเลือกตั้ง</th>
            <th>คะแนน</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['candidate_number'] ?></td>
                <td>
                    <img src="<?= url($item['candidate_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['candidate_firstname'] ?></td>
                <td><?= $item['candidate_lastname'] ?></td> 
                <td><?= isset($item['score']) ? $item['score'] : 0 ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="4">ไม่ประสงค์ลงคะแนน</td>
            <td>
                <?= isset($score_not_vote['score_not_vote']) ? $score_not_vote['score_not_vote'] : 0 ?>
            </td>
        </tr>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'คะแนนเลือกตั้ง';
require ROOT . '/admin/layout.php';
