<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$vote_id = get('vote');
$page_path = "/admin/votes/edit.php?vote={$vote_id}";

$action = get('action');
$id = get('id');

switch ($action) { 
    case 'delete':
        DB::delete('candidates', "`candidate_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if (post('vote_name')) {
    $result = DB::update('votes', [
        'vote_name' => post('vote_name'),
        'start_time' => post('start_time'),
        'end_time' => post('end_time')
    ], "`vote_id`='{$vote_id}'");

    if ($result) {
        setAlert('success', "เพิ่มเลือกตั้งสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มเลือกตั้งได้");
    }
    redirect($page_path);
}

if (post('candidate_number')) {
    $img = upload('candidate_img');
    if ($img == false) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดไฟล์ได้");
        redirect($page_path);
    }

    $result = DB::insert('candidates', [
        'vote_id' => $vote_id,
        'candidate_number' => post('candidate_number'),
        'candidate_firstname' => post('candidate_firstname'),
        'candidate_lastname' => post('candidate_lastname'),
        'candidate_img' => $img
    ]);

    if ($result) {
        setAlert('success', "เพิ่มผู้ลงเลือกตั้งสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มผู้ลงเลือกตั้งได้");
    }
    redirect($page_path);
}

$time = time();
$data = DB::row("SELECT * FROM `votes` WHERE `vote_id`='{$vote_id}'");
$items = DB::result("SELECT * FROM `candidates` WHERE `vote_id`='{$vote_id}'");
ob_start();
?>

<a href="<?= url("/admin/votes/list.php") ?>">
    <button>< กลับ</button>
</a>

<?= showAlert() ?>
<h3>แก้ไขเลือกตั้ง</h3>
<form method="post">
    <label for="vote_name">ชื่อเลือกตั้ง</label>
    <input type="text" name="vote_name" id="vote_name" value="<?= $data['vote_name'] ?>" required>
    <br>

    <label for="start_time">วันเวลาเริ่มเลือกตั้ง</label>
    <input type="datetime-local" name="start_time" id="start_time" value="<?= $data['start_time'] ?>" required>
    <br>

    <label for="end_time">วันเวลาสิ้นสุดเลือกตั้ง</label>
    <input type="datetime-local" name="end_time" id="end_time" value="<?= $data['end_time'] ?>" required>
    <br>

    <button type="submit">บันทึก</button>
</form>

<h3>เพิ่มผู้ลงเลือกตั้ง</h3>
<form method="post" enctype="multipart/form-data">
    <label for="candidate_number">หมายเลข</label>
    <input type="number" name="candidate_number" id="candidate_number" required>
    <br>

    <label for="candidate_firstname">ชื่อ</label>
    <input type="text" name="candidate_firstname" id="candidate_firstname" required>
    <br>

    <label for="candidate_lastname">นามสกุล</label>
    <input type="text" name="candidate_lastname" id="candidate_lastname" required>
    <br>

    <label for="candidate_img">ภาพผู้ลงเลือกตั้ง</label>
    <input type="file" name="candidate_img" id="candidate_img" required>
    <br>

    <button type="submit">บันทึก</button>
</form>

<h3>รายการผู้ลงเลือกตั้ง</h3>
<table>
    <thead>
        <tr>
            <th>หมายเลข</th>
            <th>ภาพผู้ลงเลือกตั้ง</th>
            <th>ชื่อ</th>
            <th>นามสกุล</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['candidate_number'] ?></td>
                <td>
                    <img src="<?= url($item['candidate_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['candidate_firstname'] ?></td>
                <td><?= $item['candidate_lastname'] ?></td>
                <td>
                    <a href="<?= url("/admin/votes/edit-candidate.php?vote={$data['vote_id']}&candidate={$item['candidate_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?= url("/admin/votes/edit.php?vote={$data['vote_id']}") ?>&action=delete&id=<?= $item['candidate_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบหมายเลข {$item['candidate_number']} หรือไม่") ?>
                    >
                        ลบ
                    </a> 
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขเลือกตั้ง';
require ROOT . '/admin/layout.php';
