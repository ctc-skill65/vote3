<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/votes/list.php";

$action = get('action');
$id = get('id');

switch ($action) { 
    case 'delete':
        DB::delete('votes', "`vote_id`='{$id}'");
        break;
}

if ($action) {
    redirect($page_path);
}

if (post('vote_name')) {
    $result = DB::insert('votes', [
        'vote_name' => post('vote_name'),
        'start_time' => post('start_time'),
        'end_time' => post('end_time')
    ]);

    if ($result) {
        setAlert('success', "เพิ่มเลือกตั้งสำเร็จเรียบร้อย");
        $vote_id = DB::insert_id();
        redirect("/admin/votes/edit.php?vote={$vote_id}");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มเลือกตั้งได้");
    }
    redirect($page_path);
}

$time = time();
$items = DB::result("SELECT * FROM `votes`");
ob_start();
?>
<?= showAlert() ?>
<h3>เพิ่มเลือกตั้ง</h3>
<form method="post">
    <label for="vote_name">ชื่อเลือกตั้ง</label>
    <input type="text" name="vote_name" id="vote_name" required>
    <br>

    <label for="start_time">วันเวลาเริ่มเลือกตั้ง</label>
    <input type="datetime-local" name="start_time" id="start_time" required>
    <br>

    <label for="end_time">วันเวลาสิ้นสุดเลือกตั้ง</label>
    <input type="datetime-local" name="end_time" id="end_time" required>
    <br>

    <button type="submit">บันทึก</button>
</form>


<h3>รายการเลือกตั้ง</h3>
<table>
    <thead>
        <tr>
            <th>รหัส</th>
            <th>ชื่อเลือกตั้ง</th>
            <th>วันเวลาเริ่มเลือกตั้ง</th>
            <th>วันเวลาสิ้นสุดเลือกตั้ง</th>
            <th>สถานะ</th>
            <th>จัดการ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['vote_id'] ?></td>
                <td><?= $item['vote_name'] ?></td>
                <td><?= $item['start_time'] ?></td>
                <td><?= $item['end_time'] ?></td>
                <td>
                    <?php 
                    if ($time >= strtotime($item['start_time']) && $time <= strtotime($item['end_time'])) {
                        echo 'เปิด';
                    } else {
                        echo 'ปิด';
                    }
                    ?>
                </td>

                <td>
                    <a href="<?= url("/admin/votes/edit.php?vote={$item['vote_id']}") ?>">
                        แก้ไข
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="?action=delete&id=<?= $item['vote_id'] ?>"
                    <?= clickConfirm("คุณต้องการลบ {$item['vote_name']} หรือไม่") ?>
                    >
                        ลบ
                    </a> 
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'จัดการเลือกตั้ง';
require ROOT . '/admin/layout.php';
