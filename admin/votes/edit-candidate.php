<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$vote_id = get('vote');
$candidate_id = get('candidate');
$page_path = "/admin/votes/edit-candidate.php?vote={$vote_id}&candidate={$candidate_id}";

if (post('candidate_number')) {
    if (!empty($_FILES['candidate_img']['name'])) {
        $img = upload('candidate_img');
        if ($img == false) {
            setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดไฟล์ได้");
            redirect($page_path);
        }

        DB::update('candidates', [
            'candidate_img' => $img
        ], "`candidate_id`='{$candidate_id}'");
    }
    

    $result = DB::update('candidates', [
        'candidate_number' => post('candidate_number'),
        'candidate_firstname' => post('candidate_firstname'),
        'candidate_lastname' => post('candidate_lastname')
    ], "`candidate_id`='{$candidate_id}'");

    if ($result) {
        setAlert('success', "แก้ไขผู้ลงเลือกตั้งสำเร็จเรียบร้อย");
        redirect("/admin/votes/edit.php?vote={$vote_id}");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขผู้ลงเลือกตั้งได้");
    }
    redirect($page_path);
}

$data = DB::row("SELECT * FROM `candidates` WHERE `candidate_id`='{$candidate_id}'");
ob_start();
?>

<a href="<?= url("/admin/votes/edit.php?vote={$vote_id}") ?>">
    <button>< กลับ</button>
</a>

<?= showAlert() ?>

<form method="post" enctype="multipart/form-data">
    <label for="candidate_img">
        <img src="<?= url($data['candidate_img']) ?>" alt="" style="
            max-height: 14rem;
        ">
    </label>
    <br>

    <label for="candidate_number">หมายเลข</label>
    <input type="number" name="candidate_number" id="candidate_number" value="<?= $data['candidate_number'] ?>" required>
    <br>

    <label for="candidate_firstname">ชื่อ</label>
    <input type="text" name="candidate_firstname" id="candidate_firstname" value="<?= $data['candidate_firstname'] ?>" required>
    <br>

    <label for="candidate_lastname">นามสกุล</label>
    <input type="text" name="candidate_lastname" id="candidate_lastname" value="<?= $data['candidate_lastname'] ?>" required>
    <br>

    <label for="candidate_img">ภาพผู้ลงเลือกตั้ง</label>
    <input type="file" name="candidate_img" id="candidate_img">
    <br>

    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขผู้ลงเลือกตั้ง';
require ROOT . '/admin/layout.php';
