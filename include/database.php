<?php

class DB 
{
    static $conn;

    static function connect() 
    {
        self::$conn = new mysqli(
            conf('db_host'),
            conf('db_user'),
            conf('db_password'),
            conf('db_name')
        );

        if (self::$conn->connect_error) {
            exit(self::$conn->connect_error);
        }

        self::$conn->set_charset(conf('db_charset'));
    }

    static function query($sql)
    {
        return self::$conn->query($sql);
    }

    static function row($sql) 
    {
        $result = self::query($sql);
        if ($result == false) {
            return null;
        }

        return $result->fetch_assoc();
    }

    static function result($sql) 
    {
        $result = self::query($sql);
        if ($result == false) {
            return null;
        }

        $items = [];
        while ($row = $result->fetch_assoc()) {
            $items[] = $row;
        }

        return $items;
    }

    static function insert($table, $data) 
    {
        $keys = [];
        $values = [];

        foreach ($data as $key => $value) {
            $keys[] = "`{$key}`";
            $values[] = "'{$value}'";
        }

        $key_sql = implode(',', $keys);
        $value_sql = implode(',', $values);

        $sql = "INSERT INTO `{$table}`({$key_sql}) VALUES ({$value_sql})";

        return self::query($sql);
    }

    static function insert_id() 
    {
        return self::$conn->insert_id;
    }

    static function update($table, $data, $where) 
    {
        $data_arr = [];

        foreach ($data as $key => $value) {
            $data_arr[] = "`{$key}`='{$value}'";
        }

        $data_sql = implode(',', $data_arr);

        $sql = "UPDATE `{$table}` SET {$data_sql} WHERE {$where}";

        return self::query($sql);
    }

    static function delete($table, $where) 
    {
        $sql = "DELETE FROM `{$table}` WHERE {$where}";

        return self::query($sql);
    }
}

DB::connect();
