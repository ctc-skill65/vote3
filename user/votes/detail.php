<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$vote_id = get('id');
$page_path = "/user/votes/detail.php?id={$vote_id}";

if ($_POST) {
    $candidate_id = post('candidate_id');

    if ($candidate_id === 'not_vote') {
        $result = DB::insert('vote_actions', [
            'user_id' => $user_id,
            'vote_id' => $vote_id,
            'not_vote' => 1
        ]);
    } else {
        $result = DB::insert('vote_actions', [
            'user_id' => $user_id,
            'vote_id' => $vote_id,
            'candidate_id' => $candidate_id,
            'not_vote' => 0
        ]);
    }

    if ($result) {
        setAlert('success', "ลงคะแนนเลือกตั้งสำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถลงคะแนนเลือกตั้งได้");
    }
    redirect($page_path);
}

$time = time();
$data = DB::row("SELECT * FROM `votes` WHERE `vote_id`='{$vote_id}'");

if ($time >= strtotime($data['start_time']) && $time <= strtotime($data['end_time'])) {
    $vote_status = true;
} else {
    $vote_status = false;
}

$items = DB::result("SELECT * FROM `candidates` WHERE `vote_id`='{$vote_id}'");

$is_action = false;
$action = DB::row("SELECT * FROM `vote_actions` WHERE `user_id`='{$user_id}' AND `vote_id`='{$vote_id}'");
if (!empty($action)) {
    $is_action = true;
}

ob_start();
?>
<?= showAlert() ?>
<h3>รายละเอียด</h3>
<p>
    เลือกตั้ง: <?= $data['vote_name'] ?>
    <br>
    วันเวลาเริ่มเลือกตั้ง: <?= $data['start_time'] ?>
    <br>
    วันเวลาสิ้นสุดเลือกตั้ง: <?= $data['end_time'] ?>
    <br>
    สถานะ: <?= $vote_status ? 'เปิด' : 'ปิด' ?>
</p>

<h3>รายการผู้ลงเลือกตั้ง</h3>
<table>
    <thead>
        <tr>
            <th>หมายเลขผู้ลงเลือกตั้ง</th>
            <th>ภาพผู้ลงเลือกตั้ง</th>
            <th>ชื่อผู้ลงเลือกตั้ง</th>
            <th>นามสกุลผู้ลงเลือกตั้ง</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
            <tr>
                <td><?= $item['candidate_number'] ?></td>
                <td>
                    <img src="<?= url($item['candidate_img']) ?>" alt="" style="
                        max-height: 8rem;
                    ">
                </td>
                <td><?= $item['candidate_firstname'] ?></td>
                <td><?= $item['candidate_lastname'] ?></td> 
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<h3>ลงคะแนนเลือกตั้ง</h3>
<?php
if ($is_action) {
    ?>
    <h1>คุณลงคะแนนเลือกตั้งแล้ว</h1>
    <?php
} elseif (!$vote_status) {
    ?>
    <h1>สถานะเลือกตั้ง ปิด</h1>
    <?php
} else {
    ?>
    <form method="post">
        <table>
            <thead>
                <tr>
                    <th>หมายเลขผู้ลงเลือกตั้ง</th>
                    <th>ภาพผู้ลงเลือกตั้ง</th>
                    <th>ชื่อผู้ลงเลือกตั้ง</th>
                    <th>นามสกุลผู้ลงเลือกตั้ง</th>
                    <th>เลือกผู้ลงเลือกตั้ง</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item) : ?>
                    <tr>
                        <td><?= $item['candidate_number'] ?></td>
                        <td>
                            <img src="<?= url($item['candidate_img']) ?>" alt="" style="
                                max-height: 8rem;
                            ">
                        </td>
                        <td><?= $item['candidate_firstname'] ?></td>
                        <td><?= $item['candidate_lastname'] ?></td>
                        <td>
                            <input type="radio" name="candidate_id" id="candidate_id<?= $item['candidate_id'] ?>" 
                                value="<?= $item['candidate_id'] ?>" 
                                style="
                                    transform: scale(2);
                                "
                                required
                            >
                            <label for="candidate_id<?= $item['candidate_id'] ?>">เลือก</label>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4">ไม่ประสงค์ลงคะแนน</td>
                    <td>
                        <input type="radio" name="candidate_id" id="not_vote" 
                            value="not_vote" 
                            style="
                                transform: scale(2);
                            "
                            required
                        >
                        <label for="not_vote">เลือก</label>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <button type="submit">บันทึก</button>
    </form>
    <?php
}
?>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายละเอียดเลือกตั้ง';
require ROOT . '/user/layout.php';
